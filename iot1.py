# Import KafkaProducer from Kafka library
from kafka import KafkaProducer
from json import dumps
from scipy.stats import truncnorm
from scipy.stats import gamma
from dtw import *
import time, json, re, os, random, datetime, sys, math, psycopg2

# Define server with port
bootstrap_servers = ['kafka-0.kafka-headless.default.svc.cluster.local:9092'] 
#bootstrap_servers = ['localhost:9092'] #(Switch to this for local runs)

# Define topic name where the message will publish. (Sample is unused now, so this is commented out. May reuse sample in the future for testing purposes)
#topicName = 'sample'

# Initialize producer variable
producer = KafkaProducer(bootstrap_servers = bootstrap_servers,value_serializer=lambda x: dumps(x).encode('utf-8'))

#print(sys.argv)
if len(sys.argv) > 1:
  
  #Starting off by the ADL-related section.
  subject_arg = ''
  if sys.argv[1] == 'adl' or sys.argv[1] == 'ADL' or sys.argv[1] == 'classify':
    subject_arg = sys.argv[2]

    #Argument checking.
    if subject_arg[0] != 'm' and subject_arg[0] != 'f':
      print("Invalid arguments. Choose between m or f.")
      sys.exit()
    
    if subject_arg[0] == 'm':
      if subject_arg[1:].isnumeric() != True:
        print("Invalid arguments. Male subjects are between 1 and 11")
        sys.exit()
      if int(subject_arg[1:]) < 0 or int(subject_arg[1:]) > 11:
        print("Invalid arguments. Male subjects are between 1 and 11")
        sys.exit()

    if subject_arg[0] == 'f':
      if subject_arg[1:].isnumeric() != True:
        print("Invalid arguments. Female subjects are between 1 and 5")
        sys.exit()
      if int(subject_arg[1:]) < 0 or int(subject_arg[1:]) > 5:
        print("Invalid arguments. Female subjects are between 1 and 5")
        sys.exit()

    #Here we remove pre-existing data from the tables we need to avoid duplicates.
    conn = psycopg2.connect(
    host="tsdb-1.default.svc.cluster.local",
    database="iotdb",
    user="postgres",
    password="psqlpass")

    #Adl_acc_01 table
    query = 'SELECT count(*) > 0 FROM adl_acc_01 WHERE volunteer = %s'
    user = subject_arg
    cur = conn.cursor()
    cur.execute(query, (user,))
    query_results = cur.fetchall()
    if query_results[0][0] == True:
      print('This volunteer has already recorded their data. Deleting the existing records for a fresh deployment...')
      cur = conn.cursor()
      query = 'DELETE FROM adl_acc_01 WHERE volunteer = %s'
      cur.execute(query, (user,))
      conn.commit()

    #finishing_times_01 table
    query = 'SELECT count(*) > 0 FROM finishing_times_01 WHERE volunteer = %s'
    cur = conn.cursor()
    cur.execute(query, (user,))
    query_results = cur.fetchall()
    if query_results[0][0] == True:
      print('This volunteer has already recorded their finishing times. Deleting those values to avoid duplicates...')
      cur = conn.cursor()
      query = 'DELETE FROM finishing_times_01 WHERE volunteer = %s'
      cur.execute(query, (user,))
      conn.commit()

    #Tables used only for the classification.
    if sys.argv[1] == 'classify':
      
      query = 'SELECT count(*) > 0 FROM adl_classify_1 WHERE volunteer = %s'
      cur = conn.cursor()
      cur.execute(query, (user,))
      query_results = cur.fetchall()
      if query_results[0][0] == True:
        print('This volunteer has already recorded their segmented data. Deleting those values to avoid duplicates...')
        cur = conn.cursor()
        query = 'DELETE FROM adl_classify_1 WHERE volunteer = %s'
        cur.execute(query, (user,))
        conn.commit()

      query = 'SELECT count(*) > 0 FROM adl_classify_matches_2 WHERE volunteer = %s'
      cur = conn.cursor()
      cur.execute(query, (user,))
      query_results = cur.fetchall()
      if query_results[0][0] == True:
        print('This volunteer has already recorded their segment matches. Deleting those values to avoid duplicates...')
        cur = conn.cursor()
        query = 'DELETE FROM adl_classify_matches_2 WHERE volunteer = %s'
        cur.execute(query, (user,))
        conn.commit()

      query = 'SELECT count(*) > 0 FROM adl_classify_labels_2 WHERE volunteer = %s'
      cur = conn.cursor()
      cur.execute(query, (user,))
      query_results = cur.fetchall()
      if query_results[0][0] == True:
        print('This volunteer has already recorded their segment labels. Deleting those values to avoid duplicates...')
        cur = conn.cursor()
        query = 'DELETE FROM adl_classify_labels_2 WHERE volunteer = %s'
        cur.execute(query, (user,))
        conn.commit()


    #Now moving on to store the files of a given subject/volunteer.
    subject_files = []

    #Here we add every file of a subject, along with the timestamp of each file in order to sort the files according to their dates.
    for folder in os.listdir("/opt/vb/ADL_dataset"):
      nested_dir = "/opt/vb/ADL_dataset/" + folder  
      for filename in os.listdir(nested_dir):
        #print(filename)

        name_split = filename.split('-')
        subject = name_split[8].replace('.txt','')
      
        if subject == subject_arg:
      
          date = ''
          for date_elem in name_split[1:7]:
            date += date_elem + '-'
          date = date[:-1]
          #date = name_split[1] + '-' + name_split[2] + '-' + name_split[3] + '-' + name_split[4] + '-' + name_split[5] + '-' + name_split[6]
      
          date_obj = datetime.datetime.strptime(date, "%Y-%m-%d-%H-%M-%S")
          filename = nested_dir + '/' + filename
          subject_files.append((filename, date_obj))
          subject_files = sorted(subject_files, key=lambda x: x[1])
        
    #print(subject_files)
    #print(len(subject_files))

    #counter = 0

    #Now, we take every file of the subject, we open it and send its data to the database and the iot-2 device. Keep in mind the iot-1 device does not send the ADL activity in the database. Also, the segment-related stuff are for the classify mode.
    segments = []
    segment_string = 'Segment'
    segment_num = 1
    segment_id = segment_string + str(segment_num)
    for items in subject_files:
      filename = items[0]
      date_obj = items[1]
      fileopen = open(filename)
  
      hz_counter = 0
      finishing_timeset = {}
      segment_counter = 0
      segment_array = []
      for line in fileopen:

        axes = line.split(' ') #Getting the accelerometer data
        axes[2] = axes[2].replace('\n','')
    
        name_split = filename.split('-')
        subject = name_split[8].replace('.txt','')

        activity = name_split[7]

        date = date_obj.strftime("%Y-%m-%dT%H:%M:%S")#Here we add the millisecond precision in the date, because we need to increment every line's data with 32 hz (0.03125 seconds) 
        date = date + '.0'
        datetime_object = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%f')
        datetime_object = datetime_object + datetime.timedelta(seconds=hz_counter)
        date = datetime_object.strftime('%Y-%m-%dT%H:%M:%S.%f')
      
        hz_counter = hz_counter + 0.03125 #Adding 0.03125 because of the 32 hz output rate

        #Sending data to the DB (for ADL mode option)
        db_set = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "x", "type": "int32", "optional": False },{"field": "y", "type": "int32", "optional": False },{"field": "z", "type": "int32", "optional": False },{"field": "volunteer", "type": "string", "optional": False },{"field": "time", "type": "string", "optional": False }]},"payload": {"x":int(axes[0]),"y":int(axes[1]),"z":int(axes[2]),"volunteer":subject,"time":date}} 
        if sys.argv[1] == 'adl' or sys.argv[1] == 'ADL':
          producer.send('adl_acc_01', db_set)
      
        #Sending data to iot-2 device. This device will send the activity label if the date received by iot-1 is one of the activity's finals (for the given subject).
        iot2_data = {'filename':filename, 'date': date, 'x':int(axes[0]),'y':int(axes[1]),'z':int(axes[2])}
        if sys.argv[1] == 'adl' or sys.argv[1] == 'ADL':
          producer.send('adl_mid_topic', iot2_data)

        #This set will be used to save the finishing times of each file in the database. This is needed during classification, to find the labels we matched as well as some other data.
        finishing_timeset = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "x", "type": "int32", "optional": False },{"field": "y", "type": "int32", "optional": False },{"field": "z", "type": "int32", "optional": False },{"field": "activity", "type": "string", "optional": False },{"field": "volunteer", "type": "string", "optional": False },{"field": "time", "type": "string", "optional": False }]},"payload": {"x":int(axes[0]),"y":int(axes[1]),"z":int(axes[2]), "activity":activity,"volunteer":subject,"time":date}} 

        #Segment data will be send during classify mode.
        segment_data_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "x", "type": "int32", "optional": False },{"field": "y", "type": "int32", "optional": False },{"field": "z", "type": "int32", "optional": False },{"field": "volunteer", "type": "string", "optional": False },{"field": "segment", "type": "string", "optional": False },{"field": "time", "type": "string", "optional": False }]},"payload": {"x":int(axes[0]),"y":int(axes[1]),"z":int(axes[2]),"volunteer":subject,"segment":segment_id, "time":date}} 
        if sys.argv[1] == 'classify':
          producer.send('adl_classify_1', segment_data_db)

        #We also keep the segment data in an array for the classification performed later on.
        segment_data = (int(axes[0]),int(axes[1]), int(axes[2]),segment_id,date,activity) 
        segment_array.append(segment_data)
        
        #480 is given by 32 * 15 (We need to segment the signal into 15 seconds and each second is made up by 32 * 0.03125)
        if segment_counter == 480: #So, whenever we have 15 sec worth of data we keep this segment and add it in the array, and updated the vars for the next segment.
          segments.append(segment_array)  
          segment_counter = 0
          segment_array = []
          segment_num += 1
          segment_id = segment_string + str(segment_num)


        segment_counter += 1

      #We also add a new segment whenever a files ends. Regardless if it's in a 15 sec range or not.
      segments.append(segment_array)
      segment_num += 1
      segment_id = segment_string + str(segment_num)
        
      fileopen.close()
      #Finishing times are sent only in ADL mode (we need the times of the volunteers to match the one to classify)
      if sys.argv[1] == 'adl' or sys.argv[1] == 'ADL':
        producer.send('finishing_times_01', finishing_timeset)

    #print(segment_id)
    print('All data have been saved to the Timescale Database.')

    #Now we classify the segments.
    if sys.argv[1] == 'classify':
      print('Moving on to classifying the recorded segments.')
      
      range_1 = range(1, 12)
      list_1 = list(range_1)
      str_list1 = []
      for i in list_1:
          str_list1.append('m'+str(i))

      range_2 = range(1, 6)
      list_2 = list(range_2)
      str_list2 = []
      for i in list_2:
          str_list2.append('f'+str(i))

      vols = str_list1+str_list2

      adl_segments = []
      finish_times_all = []
      #We get all the ADL data from the recorded volunteers and we segment their signals based on their finishing times. (So, each segment is one file from the already recorded users)
      for item in vols:
        if item == subject_arg:
          continue

        user = item

        finish_times_q = 'SELECT * FROM finishing_times_01 WHERE volunteer = %s order by time;'
        cur = conn.cursor()
        cur.execute(finish_times_q, (user,))
        finish_times = cur.fetchall()
      
        finish_dates = []
        for f in finish_times:
          finish_dates.append(f[5])
          finish_times_all.append(f)

        query = 'SELECT * FROM adl_acc_01 WHERE volunteer = %s order by time;'
        cur = conn.cursor()
        cur.execute(query, (user,))
        query_results = cur.fetchall()
    
        adl_segment = []
        for q in query_results:
          adl_segment.append(q)
          if q[4] in finish_dates:
            adl_segments.append(adl_segment) 
            adl_segment = []

      #Now that we also have the segments of the recorded users we move on to classify each segment of the selected user.
      segment_match_num = 1
      for segment in segments:

        segment_axes = [(a, b, c) for a, b, c, d, e, f in segment]
        comparisons = []
        label_true = segment[0][5]
        segment_id = segment[0][3]

        segment_match_string = 'SegmentMatch'
        segment_match_id = segment_match_string + str(segment_match_num)
        
        #Comparing each segment with every 'recorded' segment.
        for adl_segment in adl_segments:

          adl_segment_axes = [(a, b, c) for a, b, c, d, e in adl_segment]
          adl_segment_times = [(a, b, c, e) for a, b, c, d, e in adl_segment]
          time_keep = adl_segment[-1][-1]

          ds = dtw(segment_axes, adl_segment_axes,keep_internals=True)
          diff = round(ds.normalizedDistance,2)
          comparison = (adl_segment_times, diff, time_keep)
          comparisons.append(comparison)

        #We sort the comparisons by the returned difference to get the closest matching signal.
        sort_comparisons = sorted(comparisons, key=lambda x: x[1])
        match = sort_comparisons[0]          
        match_time = match[2]
        diff = match[1]
        label_match = ''
        vol_match = ''

        #Now we also sort all the finishing times to find the label and volunteer of the recorded segment.
        sort_times = sorted(finish_times_all, key=lambda x: x[5])

        #Getting label and volunteer and sending the related data to the adl_classify_labels_2 table.
        for f in sort_times:
          if f[5] >= match_time:
            label_match = f[3]
            vol_match = f[4]
            label_data_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "volunteer", "type": "string", "optional": False },{"field": "volunteer_match", "type": "string", "optional": False },{"field": "segment_query", "type": "string", "optional": False },{"field": "segment_match", "type": "string", "optional": False },{"field": "label_true", "type": "string", "optional": False },{"field": "label_match", "type": "string", "optional": False },{"field": "difference", "type": "float", "optional": False },]},"payload": {"volunteer":subject_arg,"volunteer_match":vol_match,"segment_query":str(segment_id), "segment_match":segment_match_id, "label_true": label_true,"label_match":label_match, "difference": float(diff)}}
            producer.send('adl_classify_labels_2',label_data_db)
            break
        
        #And here we sent the entire signal in the adl_classify_matches_2 table.
        match_array = match[0]
        for value in match_array:
          (x,y,z, time_r) = value          

          time_s = time_r.strftime('%Y-%m-%dT%H:%M:%S.%f')

          match_data_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "x", "type": "int32", "optional": False },{"field": "y", "type": "int32", "optional": False },{"field": "z", "type": "int32", "optional": False },{"field": "volunteer", "type": "string", "optional": False },{"field": "volunteer_match", "type": "string", "optional": False },{"field": "segment_query", "type": "string", "optional": False },{"field": "segment_match", "type": "string", "optional": False },{"field": "label_true", "type": "string", "optional": False },{"field": "label_match", "type": "string", "optional": False },{"field": "difference", "type": "float", "optional": False },{"field": "time", "type": "string", "optional": False }]},"payload": {"x":int(x),"y":int(y),"z":int(z),"volunteer":subject_arg,"volunteer_match":vol_match,"segment_query":str(segment_id), "segment_match":segment_match_id, "label_true": label_true,"label_match":label_match,"difference":float(diff) ,"time":time_s}}
          producer.send('adl_classify_matches_2',match_data_db)

        segment_match_num += 1
        print('Segment ' + segment_id + ' written out.')


    #Keeping the container running for k8s.    
    while True:
      time.sleep(5)
      print('Keeping the container running.')

  
  #The code that follows is for random (non-ADL) data.  
  elif sys.argv[1] == 'random':
    
    #Getting 1000 values based on the Gamma distribution for temperature. 
    def get_arr(): 
      temp_arr = gamma.rvs(a = 2.0, scale = 2.0, size=1000)
      temp_arr = list(filter( lambda x: 40.0>x>2.0, temp_arr )) #Using filter to truncate the values. This removes some elements but I couldn't find any better way to truncate the Gamma dist.
      temp_arr = [ '%.2f' % elem for elem in temp_arr ] #Double decimal
      return temp_arr

    #Getting a temperature array
    temp_arr = get_arr()
    temp_size = len(temp_arr)

    #Used just in case we need to flush the producer's buffer
    #producer.flush()

    temp_counter = 0 #Used to get the next value of the temperature
    time_counter = 0 #Tracks time in order to change the open door status
    open_door = True #Open door starts as true

    while True:
      #start = time.time()

      #Producer 
      time.sleep(5)
      time_counter += 5

      #Open door switching.
      if time_counter > 30: #Open door may update after every 30 sec
        prev_door = open_door
        coin_flip = random.choice([True, False])
        if coin_flip == False:
          open_door = False
        else:
          open_door == True
        if open_door != prev_door:
          time_counter = 0

      if time_counter == 60:  #Must always update after 60 sec
        open_door = not open_door
        time_counter = 0

      #Sending the open door status to the 'sample3' topic of Analytics device.
      print("Sending open door status (",open_door,") to Analytics...")
      door_set = {"topic":'door',"open_door": open_door} 
      producer.send('sample3', door_set)

      if open_door == True:

        temp_item = temp_arr[temp_counter] #Getting next temperature value
        temp_counter += 1

        if temp_counter == temp_size: #Update the array when the limit of the previous array is reached
          temp_arr = get_arr()
          temp_counter = 0
        
        
        #Sending temperature to IoT_2 (mid_sample) and Analytics (sample2)
      #  ts = time.time()
        temp_set = {"topic":'temp',"temperature": temp_item}
        temp_set_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "temperature", "type": "float", "optional": False }]},"payload": {"temperature": float(temp_item)}} 
        print('Sending temperature (', temp_item ,') to IoT_2 and Analytics... ')
        producer.send('temp_01', temp_set_db)
        producer.send('mid_sample',temp_set)
        producer.send('sample2', temp_set)
      else:
        print('Door is closed... ')
        
      #end = time.time()
      #print(end-start)


    # Used for testing
    print("Message Sent")
    producer.close()
  else:
    print("Invalid arguments. Choose between adl/ADL, classify or random")
    sys.exit()
