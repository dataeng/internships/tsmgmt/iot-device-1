FROM ubuntu:latest
RUN apt-get update && apt-get -y install python3 && apt-get -y install python3-pip
RUN pip3 install scipy
RUN pip3 install kafka-python
RUN pip3 install psycopg2-binary
RUN pip3 install dtw-python
COPY ./ /opt/vb
ENV PYTHONUNBUFFERED=1
ENTRYPOINT ["python3", "/opt/vb/iot1.py"]
