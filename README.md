# iot-device-1

Simulated IoT device

This device has three modes, 'ADL', 'classify' and 'random'. The 'ADL' mode will send the data of the ADL dataset to the Timescale DB and the IoT_2 device to emit the labels.

Classify mode finds the closest matching signals of a segmented signal from one user and sends the related data to the Timescale DB.

For the random data, the device generates temperature values that are sent to IoT_2 and Analytics devices. Also, it has a door whose open-or-close status changes over time. At every 30 seconds the device will randomly choose whether to change the open door status or not. Also, at every 60 seconds the status will forcibly change. When the door is closed, data can't be read from it.

In order to send the data, the device sleeps for five seconds and wakes up to update the open door and send its status to Analytics device (in 'sample3'). After that, if the door is open, it will choose a value from an array of Gamma-distributed temperature values and send it over to the IoT_2 (in the 'mid_sample' topic) and Analytics (in 'sample2') devices. Also, the device will send the same temperature value in the 'temp_01' topic to be consumed from the Timescale DB (for more on that, check the database-recorder repo). When the device reaches the last value of the array, a new array with fresh values will be generated. If the door is closed, it will send the closed door status.

Details about the installation of Kubernetes and Kafka can be seen Kubernetes-Kafka-Installation.txt. For how to deploy a python script as k8s pod, see: Kubernetes-Deployment-with-GitLab-CI.txt

How to use the device: Originally, the deployment of the device was done by the CI/CD system of GitLab. Now that there are three modes, the deployment is done by the CLI (since we may need different arguments each time). If you want to deploy the device and use it on the ADL data you will need to type the arguments: 'adl' and the volunteer id (m1-11 and f1-5). For the classify mode you need the word 'classify' and the volunteer id too. Make sure not to type the id of an already recorded user. For the random data, you'll need the 'random' argument. Here's an example use of it: `kubectl run iot1 --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-1:<latest-tag> --overrides='{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }' --command -- python3 /opt/vb/iot1.py adl f1`.

Keep in mind that whenever you update the device, you will need to add a new tag.

If you want to revert to the deployment through GitLab, update the Dockerfile and change the line: `ENTRYPOINT ["python3", "/opt/vb/iot1.py"]` to `CMD ["python3", "/opt/vb/iot1.py"]`, add the arguments you need, and uncomment the 'deploy' job of the .gitlab-ci.yml file.
