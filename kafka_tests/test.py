#Test for sending data to the Timescale database.

# Import KafkaProducer from Kafka library
from kafka import KafkaProducer
from json import dumps
import time
import json
import re
import random

# Define server with port
bootstrap_servers = ['kafka-0.kafka-headless.default.svc.cluster.local:9092']

# Define topic name where the message will publish
topicName = 'sample'

# Initialize producer variable
producer = KafkaProducer(bootstrap_servers = bootstrap_servers,value_serializer=lambda x: 
                         dumps(x).encode('utf-8'))

producer.flush()
#producer.send(topicName, b'Hello from kafka...')

counter=0
while True:
    time.sleep(5)
    counter += 1
    data = { "schema": {"type": "struct", "optional": False, "version": 1, "fields": [{ "field": "number", "type": "int32", "optional": True }]},"payload": {"number": counter}}
    #data2 = {'number' : counter*8}
    producer.send(topicName, value=data)
    #producer.send('sample2', value = temp_set)


print("Message Sent")
producer.close()
