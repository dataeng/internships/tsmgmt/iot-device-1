#Local consumer test for Python Kafka

# Import KafkaConsumer from Kafka library
from kafka import KafkaConsumer
from json import loads
import time

# Import sys module
import sys

# Define server with port
bootstrap_servers = ['localhost:9092']

# Define topic name from where the message will recieve
topicName = 'sample'

# Initialize consumer variable
consumer = KafkaConsumer(topicName,bootstrap_servers= bootstrap_servers,value_deserializer=lambda x: loads(x.decode('utf-8')))

#Read and print message from consumer
for msg in consumer: 

    time.sleep(7)
    print(msg.value)
    

